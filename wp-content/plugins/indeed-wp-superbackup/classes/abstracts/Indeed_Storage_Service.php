<?php
if (!defined('ABSPATH')) exit();
if (class_exists('IndeedDropboxV2')) return;

abstract class Indeed_Storage_Service{
    abstract public function get_list_of_files();
    abstract public function download_file($source_file = '', $target_file = '');
    abstract public function upload_file($source='', $target='');
    abstract public function delete_file($target_file='');
    abstract public function get_url_for_file($target_file = '');
    abstract public function get_logs_files();
}
