<?php
if (!defined('ABSPATH')) exit();
if (class_exists('IndeedDropboxV2')) return;
require_once IBK_PATH . 'classes/API/DropboxV2/vendor/autoload.php';
require_once IBK_PATH . 'classes/abstracts/Indeed_Storage_Service.php';
use Kunnu\Dropbox\DropboxApp;
use Kunnu\Dropbox\Dropbox;

class IndeedDropboxV2 extends Indeed_Storage_Service{
    private $_destination_id = 0;
    private $_dropbox = null;
    private $_callback_url = null;
    private $_metas = array();

    public function __construct($destination_id=0){
        if ($destination_id){
            $this->_destination_id = $destination_id;
            if (!function_exists('ibk_return_metas_from_custom_db')){
                require_once IBK_PATH . 'utilities.php';
            }
            if ($this->_destination_id){
                $this->_metas = ibk_return_metas_from_custom_db('destinations', $this->_destination_id);
            }
        }
        $this->_callback_url = IBK_URL . 'plugins/indeed-wp-superbackup/admin/dropboxv2_landing_page.php';

        if (empty($this->_metas['app_key']) || empty($this->_metas['app_secret']) || empty($this->_metas['access_token'])){
            return FALSE;
        }

        $app = new DropboxApp($this->_metas['app_key'], $this->_metas['app_secret'], $this->_metas['access_token']);
        $this->_dropbox = new Dropbox($app);
    }

    public function get_list_of_files(){
        $folder = $this->_set_full_path("");
        $listFolderContents = $this->_dropbox->listFolder($folder);
        $items = $listFolderContents->getItems();
        $files = array();
        if ($items){
            foreach ($items as $key=>$data){
                $files[] = $data->getName();
            }
        }
        return $files;
    }

    public function download_file($source_file='', $target_file=''){
        $source_file = $this->_set_full_path($source_file);
        return $this->_dropbox->download($source_file, $target_file);
    }

    public function upload_file($source='', $target=''){
        $target = $this->_set_full_path($target);
        return $this->_dropbox->simpleUpload($source, $target, []);
    }

    public function delete_file($target_file=''){
        $target_file = $this->_set_full_path($target_file);
        return $this->_dropbox->delete($target_file);
    }


    public function get_url_for_file($filename=''){
        $filename = $this->_set_full_path($filename);
        $link = $this->_dropbox->getTemporaryLink($filename)->getLink();
        return $link;
    }

    public function get_logs_files(){
      $return_arr = array();
			if ($this->_dropbox){
				$data = $this->get_list_of_files();
				if (!empty($data)){
					foreach ($data as $file){
						if (preg_match("#superbackup(.*)$#i", $file)){
							$is_log = explode('.', basename($file) );
							if (isset($is_log[1]) && $is_log[1]=='log'){
								$return_arr[] = $file;
							}
						}
					}
				}
			}
			return $return_arr;
    }

    private function _set_full_path($filename=''){
        if (!empty($this->_metas['path'])){
            $the_path = $this->_metas['path'];
            if (strpos($the_path , '/')!==-1 && (isset($filename[0]) && $filename[0]!='/')){
                $the_path .= '/';
            }
            $filename = $the_path . $filename;
        }
        if (isset($filename[0])){
            if ($filename[0]!='/') $filename = '/' . $filename;
        } else $filename = '/';
        
        return $filename;
    }

}
